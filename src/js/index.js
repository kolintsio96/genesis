import { tns } from 'tiny-slider/src/tiny-slider.module';
import 'tiny-slider/dist/tiny-slider.css';
const sliderBlockTop = document.querySelector('#sliderBlockTop'),
    sliderBlockBottom = document.querySelector('#sliderBlockBottom');

tns({
    'container': '.hero-slider',
    'items': 1,
    'speed': 500,
    'autoplayButtonOutput': false,
    'controls': false,
    'arrowKeys': false,
    'autoplay': true,
    'mode': 'gallery',
    'nav': false,
    'autoplayHoverPause': true,
    'autoplayTimeout': 5000,
    'swipeAngle': false
});

let createSlider = (className) => {
    let selector = '#' + className;
    tns({
        'container': selector,
        'items': 1,
        'nav': false,
        'slideBy': 'page',
        'mouseDrag': false,
        'autoplay': false,
        'swipeAngle': false,
        'speed': 300,

        'responsive': {
            0: {
                gutter: 15,
                items: 1,
                edgePadding: 30,
                autoHeight: true,
                controls: false,
            },
            1024: {
                gutter: 0,
                items: 1,
                edgePadding: 0,
                autoHeight: false,
                controls: true,
            },
        },
    });
};
createSlider(sliderBlockTop.id);
createSlider(sliderBlockBottom.id);